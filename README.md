# EL LABERINTO

_Este proyecto consiste en la interacción con una matriz de 12x12. El desafío es llegar desde el punto inicial (0,0) al punto final (11,11) por medio de un camino que se generará de manera aleatoria. Para avanzar a través del camino, el usuario debe presionar las teclas "W", "S", "A" y "D" para moverse hacia arriba, abajo, izquierda y derecha respectivamente._

### Instalación:

1. Descargue el archivo proyecto_1.py
2. Abra su terminal y dirijase al lugar donde está ubicado el archivo
3. Escriba python3 proyecto_1.py en su terminal para ejecutar el programa

### Instrucciones del juego:

1. Usted empezará en el punto (0,0) que será marcado por una X.
2. Para moverse a través del laberinto consiste en el camino indicado por 0, presione las teclas "W", "S", "A" y "D" para ir hacia arriba, abajo, izquierda y derecha respectivamente.
3. Después de escoger su tecla, debe presionar la tecla ENTER para ejecutar el movimiento. Su progreso será marcado por un camino de X.
4. Cuando llegue al final del laberinto (11,11), presione la tecla "R" para volver a jugar con un camino aleatorio nuevo.
5. Si quiere salir del juego, presione la tecla "S".

### PEP8 en el programa:

_La utilización de la PEP8 a lo largo de todo el código fue fundamental para el correcto funcionamiento del programa._
_Ejemplo:_
```
def recorrer_camino(movimiento):
    EJEY = 0
    EJEX = 0
    contador = 0
    copy[0][0] = "X "
    imprimir_camino(movimiento)
    while movimiento[11][11] != "X ":
        for ejey in range(100):
            copy[0][0] = "X "
            if movimiento[11][11] =="X ":
                print("\nFelicitaciones! La cantidad de movimientos realizados para llegar al final fue de:",contador)
                break
            avanzar = "lol" 
            while avanzar.upper() != "W" or avanzar.upper() != "A" or avanzar.upper() != "S" or avanzar.upper() != "D":
                avanzar = str(input("\nPresione la tecla A para avanzar hacia la izquierda, D para la derecha, W para arriba y S para abajo: "))
                print("\n")
                if avanzar.upper() == "W" or avanzar.upper() == "A" or avanzar.upper() == "S" or avanzar.upper() == "D" :
                    break
                else:
                   print("\n*Presione una tecla válida*\n")
            if (EJEY >= 0 and EJEX >= 0) or (EJEY <= 11 and EJEX <= 11) :        
                if avanzar.upper() == "W":
                    if movimiento[EJEY-1][EJEX] == "O ":
                        EJEY = EJEY-1
                        movimiento[EJEY][EJEX] = "X "
                        contador = contador +1
                    else:
                        print("\n*Mantente dentro del camino por favor =)*\n ")
                elif avanzar.upper() == "D":
                    if movimiento[EJEY][EJEX+1] == "O ":
                        EJEX = EJEX+1
                        movimiento[EJEY][EJEX] = "X "  
                        contador = contador +1
                    else:
                        print("\n*Mantente dentro del camino por favor =)*\n ")
                elif avanzar.upper() == "S":
                    if movimiento[EJEY+1][EJEX] == "O ":
                        EJEY = EJEY+1
                        movimiento[EJEY][EJEX] = "X " 
                        contador = contador +1
                    else:
                        print("\n*Mantente dentro del camino por favor =)*\n ")    
                elif avanzar.upper() == "A":
                    if movimiento[EJEY][EJEX-1] == "O ":
                        EJEX = EJEX-1
                        movimiento[EJEY][EJEX] = "X "  
                        contador = contador + 1
                    else:
                        print("\n*Mantente dentro del camino por favor =)*\n ")    
            else:
                print("*Ha salido del rectángulo*")
            print("N° DE CICLO:",ejey,"COORDENADA:",EJEY,",",EJEX,"MOVIMIENTO:",avanzar,"CONTADOR:",contador)
            print("\n")
            imprimir_camino(movimiento)
```
_Esta función representa el movimiento que hará el usuario al interactuar con el programa. Un claro ejemplo de la utilización de la PEP8 en el código, son los 4 espacios presentes después de definir una función, un ciclo o una condicional. Cada vez que se requiera definir otra estructura dentro de una estructura ya creada, se deben usar 4 espacios como lo muestra el extracto. Luego de definir la estructura a utilizar se deben escribir : y los operadores de comparación como == o booleans como "or" deben tener un espacio en blanco a cada lado._ 

### Creado con:

- GNU/Linux
- VIM
- Python3 
- GitLab

### Autores:

Patricia Fuentes González - [PatriciaFuentesG](https://gitlab.com/PatriciaFuentesG)

Magdalena Lolas Martínez - [mlolas](https://gitlab.com/mlolas)